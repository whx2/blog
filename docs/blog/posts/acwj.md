---
draft: true
date: 2024-03-28
categories:
  - C
  - Compiler
authors:
  - whx
tags:
  - C
  - Compiler
---

# acwj 编译器写作之旅

[acwj(a compiler writing journey)](https://github.com/DoctorWkt/acwj) 是一个自举实现的C语言子集编译器，本文引用并翻译了这篇文章，加入个人笔记和见解，记录个人的学习过程。

<!-- more -->

1. 00_简介
2. 01_扫描器
3. 02_解析器
4. 03_运算符优先级
5. 04_汇编代码生成
6. 05_语句
7. 06_变量语句
8. 07_比较语句
